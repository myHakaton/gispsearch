import {Carousel} from "antd";
import './index.css';
import {Slide} from "../slide";
import FingerprintJS from '@fingerprintjs/fingerprintjs'
import {useEffect, useState} from "react";


export const Widget = () => {
    const [supportMeasures,setSupportMeasures] = useState([{
        title: 'Программа ФРП "Лизинг"',
        description: 'Займы предоставляются для финансирования от 10% до 90% первоначального взноса (аванса) лизингополучателя, составляющего от 10% до 50% от стоимости приобретаемого в рамках договора промышленного оборудования'
    }, {
        title: 'Программа ФРП "Проекты развития"',
        description: 'Целевой займ на реализацию проектов, направленных на внедрение передовых технологий, создание новых продуктов или организацию импортозамещающих производств'
    }]);
    useEffect(() => {
        FingerprintJS.load()
            .then(fp => {
                return fp.get()
            })
            .then(result => {
                const visitorId = result.visitorId
                console.log('id user >>>>>> ',visitorId)
            });
    }, [])
    return (
        <Carousel autoplay dots={false}>
            {
                supportMeasures.map(item => {
                    return (
                        <Slide title={item.title} description={item.description} imgUrl={item.imgUrl}/>
                    )
                })
            }
        </Carousel>
    )
}