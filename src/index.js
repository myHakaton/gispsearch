import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './index.css';
import reportWebVitals from './reportWebVitals';
import {Widget} from "./widget";
import {MeasuresSlider} from "./slider";


const elementSidebarList = document.getElementsByClassName('sidebar')[1];
if (elementSidebarList) {
    const elementWidget = document.createElement('div');
    elementWidget.setAttribute('id', 'widget');
    elementSidebarList.appendChild(elementWidget);
    ReactDOM.render(
        <Widget/>,
        document.getElementById('widget')
    );
}

const sidebarTyumen = document.getElementsByClassName('subscribe')[0];
if (sidebarTyumen) {
    const elementWidget = document.createElement('div');
    elementWidget.setAttribute('id', 'widget');
    sidebarTyumen.parentElement.appendChild(elementWidget);
    ReactDOM.render(
        <Widget/>,
        document.getElementById('widget')
    );
}

const sidebarNalog = document.getElementsByClassName('page-content__right')[0];
if (sidebarNalog) {
    const elementWidget = document.createElement('div');
    elementWidget.setAttribute('id', 'widget');
    sidebarNalog.appendChild(elementWidget);
    ReactDOM.render(
        <Widget/>,
        document.getElementById('widget')
    );
}

window.onload  = () => {
    const sidebarGosUslugi = document.getElementsByClassName('right-bar')[0];
    if (sidebarGosUslugi) {
        const elementWidget = document.createElement('div');
        elementWidget.setAttribute('id', 'widget');
        sidebarGosUslugi.appendChild(elementWidget);
        ReactDOM.render(
            <Widget/>,
            document.getElementById('widget')
        );
    }
}


const elementMeasuresSlider = document.querySelector('.measures .flickity-enabled');
console.log(elementMeasuresSlider);
if (elementMeasuresSlider) {
    elementMeasuresSlider.innerHTML = '';
    const newMeasuresSlider = document.createElement('div');
    newMeasuresSlider.setAttribute('id', 'newMeasuresSlider');
    elementMeasuresSlider.appendChild(newMeasuresSlider);
    ReactDOM.render(
        <MeasuresSlider/>,
        document.getElementById('newMeasuresSlider')
    );
}

ReactDOM.render(
    <div>Тест</div>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
