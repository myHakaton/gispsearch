import {Card} from "antd";
import {DislikeOutlined, EllipsisOutlined, LikeOutlined, SearchOutlined} from '@ant-design/icons';
import './index.css'

export const Slide = (props) => {
    return (
        <Card className={'Slide'}
              actions={[
                  <SearchOutlined key="search"/>,
                  <LikeOutlined key="like"/>,
                  <DislikeOutlined key="dislike"/>,
                  <EllipsisOutlined key="ellipsis"/>
              ].filter(item => {
                  return !(props.bordered && item.key === 'search');

              })}
              bordered={props.bordered || false}>
            <Card.Meta title={props.title} description={props.description}/>
        </Card>
    )
}